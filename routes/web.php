<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('scrap/{geocacher}', '\App\Http\Controllers\ScrapController@index');
//Route::get('certitude/{wp}', '\App\Http\Controllers\ScrapController@certitude');
Route::get('certitude/', '\App\Http\Controllers\CertitudeController@certitude');
Route::post('certify/', '\App\Http\Controllers\CertifyController@certify');
Route::get('{id}', '\App\Http\Controllers\CertifyController@descarga');
