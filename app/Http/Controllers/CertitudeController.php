<?php namespace App\Http\Controllers;
use Goutte\Client;
use App\User;
use Auth;


class CertitudeController extends Controller
{

    function index($geocacher=""){
            $urlPerfil = $this->urlDeGeocaching('about',$geocacher);
            $encontradosHidesFavoritos = "";
            $client = new Client();
            // me logueo
            $crawler = $client->request('GET', 'https://www.geocaching.com/account/signin?returnUrl=%2fplay');
            $form = $crawler->selectButton('SignIn')->form();
            $crawler = $client->submit($form, array('UsernameOrEmail' => 'Ibonito', 'Password' => 'Rg182403'));

            // Saco las estadísticas
            $paginaPerfil = $client->request('GET', $urlPerfil);
            $estadisticas =  $paginaPerfil->filter('div[class="profile-stats"]') ;
            foreach ($estadisticas as $domElement) {
                $encontradosHidesFavoritos = ($domElement->textContent);
            }

            $usuarioId = $this->creaUser($geocacher);
            dd($usuarioId);

            // Se las envío por email
            $urlEmail           = $this->urlDeGeocaching('email',$geocacher);
            $crawlerEmail       = $client->request('GET', $urlEmail);
            $form = $crawlerEmail->selectButton('ctl00$ContentBody$SendMessagePanel1$btnSend')->form();
            $crawler = $client->submit($form, array('ctl00$ContentBody$SendMessagePanel1$tbMessage' => $encontradosHidesFavoritos));

    }

    function certitude($wp=""){
        if (isset($_GET['wp'])) $wp=$_GET['wp'];
        $client = new Client();

       // $crawler = $client->request('GET', 'https://www.certitudes.org/certitude?wp=GC8HG36');
       $crawler =  $client->request('GET', 'https://www.certitudes.org/certitude?wp='.$wp, [    //GC8HG36
          'headers' => [
              'User-Agent' =>  'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36',
          ]
      ]);

        $primeraTabla = true;
      $todos =  [];
           $misresolvedores =array();
      $crawler->filter('table[id="rk"]')->each(function ($mitabla) use(&$primeraTabla,&$todos) {
       $resolvedores =  $mitabla->filter('tr')->nextAll()->each(function ($mitr) use(&$todos,&$primeraTabla) {
            $fila = "";
            $lpostd =  $mitr->filter('td')->each(function ($mitd)  use(&$fila,&$primeraTabla) {
                //dd(trim($fila.$mitd->text()) != '' && !$primeraTabla);
                $coma = (trim($fila.$mitd->text()) != '') ? "," : "";
                $fila = $fila.$mitd->text().$coma;
                //print_r($mitd->text()."</br>");
            });
            if($fila ==" , , ,") $fila = "";
            return $fila;
        });
        if ($primeraTabla){
            $primeraTabla=false;
           // return false;
        }
        $todos[] = $resolvedores;
        $misresolvedores[] =$resolvedores;
        //return $misresolvedores;
    });

    return view('certitude.index')
    ->with('user',Auth::user())
    ->with('todos', $todos);



    // include 'funciones.php';
    // $respuesta = "";
    // if (!empty($_POST['coordinates'])){
    //   $respuesta = $_POST['coordinates'];
    //   echo compruebaRespuesta($respuesta);
    //   header('Location: /certify?resp='.compruebaRespuesta($respuesta));
    //   die();
    // }



}



public function compruebaRespuesta($respuesta){
    $respuestas = listadoRespuestas();

    if(isset($respuestas[strtoupper($respuesta)])) return $respuestas[$respuesta]['respuesta']; else return 'XX';


    //return $respuestas[$respuesta];
}



public function listadoRespuestas(){
    $soluciones=[
'12'    => ['provincia' => '      A', 'respuesta' => 'Genial!!'],
'AB'     => ['provincia' => '      AB', 'respuesta' => 'Fantástico!!'],
'AL'    => ['provincia' => '      AL', 'respuesta' => 'Otra más!!'],
'7'     => ['provincia' => '      AV', 'respuesta' => 'Vamos, ya quedan menos!!'],
'B'    => ['provincia' => '      B', 'respuesta' => 'Vamos, ya quedan menos!!'],
'BA'    => ['provincia' => '      BA', 'respuesta' => 'Genial!!'],
'BI'    => ['provincia' => '      BI', 'respuesta' => 'Fantástico!!'],
'C'    => ['provincia' => '      C', 'respuesta' => 'Otra más!!'],
'CA'    => ['provincia' => '      CA', 'respuesta' => 'Vamos, ya quedan menos!!'],
'CC'    => ['provincia' => '      CC', 'respuesta' => 'Genial!!'],
'CE'    => ['provincia' => '      CE', 'respuesta' => 'Fantástico!!'],
'CO'    => ['provincia' => '      CO', 'respuesta' => 'Otra más!!'],
'CR'    => ['provincia' => '      CR', 'respuesta' => 'Vamos, ya quedan menos!!'],
'88'    => ['provincia' => '      CS', 'respuesta' => 'Genial!!'],
'CU'    => ['provincia' => '      CU', 'respuesta' => 'Fantástico!!'],
'GC'    => ['provincia' => '      GC', 'respuesta' => 'Otra más!!'],
'GI'    => ['provincia' => '      GI', 'respuesta' => 'Vamos, ya quedan menos!!'],
'GR'    => ['provincia' => '      GR', 'respuesta' => 'Genial!!'],
'GU'    => ['provincia' => '      GU', 'respuesta' => 'Fantástico!!'],
'2'    => ['provincia' => '      H', 'respuesta' => 'Otra más!!'],
'HU'    => ['provincia' => '      HU', 'respuesta' => 'Vamos, ya quedan menos!!'],
'IB'    => ['provincia' => '      IB', 'respuesta' => 'Genial!!'],
'J'    => ['provincia' => '      J', 'respuesta' => 'Fantástico!!'],
'L'    => ['provincia' => '      L', 'respuesta' => 'Otra más!!'],
'LE'    => ['provincia' => '      LE', 'respuesta' => 'Vamos, ya quedan menos!!'],
'LO'    => ['provincia' => '      LO', 'respuesta' => 'Genial!!'],
'LU'    => ['provincia' => '      LU', 'respuesta' => 'Fantástico!!'],
'M'    => ['provincia' => '      M', 'respuesta' => 'Otra más!!'],
'MA'    => ['provincia' => '      MA', 'respuesta' => 'Vamos, ya quedan menos!!'],
'ML'    => ['provincia' => '      ML', 'respuesta' => 'Genial!!'],
'MU'    => ['provincia' => '      MU', 'respuesta' => 'Fantástico!!'],
'NA'    => ['provincia' => '      NA', 'respuesta' => 'Otra más!!'],
'O'    => ['provincia' => '      O', 'respuesta' => 'Vamos, ya quedan menos!!'],
'OU'    => ['provincia' => '      OU', 'respuesta' => 'Genial!!'],
'PO'    => ['provincia' => '      PO', 'respuesta' => 'Fantástico!!'],
'S'    => ['provincia' => '      S', 'respuesta' => 'Otra más!!'],
'SA'    => ['provincia' => '      SA', 'respuesta' => 'Vamos, ya quedan menos!!'],
'SE'    => ['provincia' => '      SE', 'respuesta' => 'Genial!!'],
'SG'    => ['provincia' => '      SG', 'respuesta' => 'Fantástico!!'],
'SO'    => ['provincia' => '      SO', 'respuesta' => 'Otra más!!'],
'SS'    => ['provincia' => '      SS', 'respuesta' => 'Vamos, ya quedan menos!!'],
'T'    => ['provincia' => '      T', 'respuesta' => 'Genial!!'],
'TE'    => ['provincia' => '      TE', 'respuesta' => 'Fantástico!!'],
'TF'    => ['provincia' => '      TF', 'respuesta' => 'Otra más!!'],
'TO'    => ['provincia' => '      TO', 'respuesta' => 'Vamos, ya quedan menos!!'],
'V'    => ['provincia' => '      V', 'respuesta' => 'Genial!!'],
'VA'    => ['provincia' => '      VA', 'respuesta' => 'Fantástico!!'],
'VI'    => ['provincia' => '      VI', 'respuesta' => 'Otra más!!'],
'Z'    => ['provincia' => '      Z', 'respuesta' => 'Vamos, ya quedan menos!!'],
'ZA'    => ['provincia' => '      ZA', 'respuesta' => 'Genial!!'],
'CUANTOS BANNERS HAY'    => ['provincia' => 'General', 'respuesta' => '5']



    ];

    return $soluciones;

}






}


