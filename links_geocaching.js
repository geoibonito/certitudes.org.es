function googlestreetviewURL(lat, lon) {
  return 'http://maps.google.com/maps?q=&layer=c&cbll=' + lat + ',' + lon;
}

function mapquestURL(lat, lon) {
  //return 'http://www.mapquest.com/maps/map.adp?latlongtype=decimal&latitude=' + lat + "&longitude=" + lon;
  return "http://www.mapquest.com/map?q=" + lat + "," + lon + "&maptype=hybrid"
}

function ftfsearchURL(state) {
  return "https://www.geocaching.com/play/search?ot=4&r=" + state + "&sort=DateLastVisited&asc=True"
}

function startingcoordsgoogleURL(coords) {
  return 'http://maps.google.com/maps?q='+coords+'&t=h';
}

function destinationcoordsgoogleURL(coords) {
  return 'http://maps.google.com/maps?q='+coords+'&t=h';
}

function drivingdirectionsgoogleURL() {
  var startlat = $('#distance_calc_starting_lat_ddd').val();
  var startlon = $('#distance_calc_starting_lon_ddd').val();
  var destlat = $('#distance_calc_destination_lat_ddd').val();
  var destlon = $('#distance_calc_destination_lon_ddd').val();
  return "https://www.google.com/maps/dir/" + startlat + "+" + startlon + "/" + destlat + "," + destlon + "/@" + destlat + "," + destlon + ",14554m/data=!3m2!1e3!4b1!4m6!4m5!1m3!2m2!1d" + startlon + "!2d" + startlat + "!1m0";
}

function projectstartingcoordsgoogleURL(coords) {
  return 'http://maps.google.com/maps?q='+coords+'&t=h';
}

function projectdestinationcoordsgoogleURL(coords) {
  return 'http://maps.google.com/maps?q='+coords+'&t=h';
}

function projectdrivingdirectionsgoogleURL() {
  var startlat = $('#projection_starting_lat_ddd').val();
  var startlon = $('#projection_starting_lon_ddd').val();
  var destlat = $('#projectedcoords_lat').val();
  var destlon = $('#projectedcoords_lon').val();
  return "https://www.google.com/maps/dir/" + startlat + "+" + startlon + "/" + destlat + "," + destlon + "/@" + destlat + "," + destlon + ",14554m/data=!3m2!1e3!4b1!4m6!4m5!1m3!2m2!1d" + startlon + "!2d" + startlat + "!1m0";
}

function theholygoogleURL(coords) {
  //return 'http://maps.google.com/maps?q=' + lat + ',' + lon + '&t=h';
  return 'http://maps.google.com/maps?q='+coords+'&t=h'
}

function trackablesURL() {
  var trackable = $('#tb').val();
  if (!trackable) {
    alert("Please enter a TB number.")
    return False;
  } else {
    return "https://www.geocaching.com/track/details.aspx?tracker=" + trackable;
  }
}

function geocachingkeywordURL() {
  var keyword = $('#kw').val();
  if (!keyword) {
    alert("Please enter a keyword.")
    return False;
  } else {
    return "https://www.geocaching.com/play/search?kw=" + keyword;
  }
}

function geocachinglogURL() {
  var gcnum = $('#gcnum').val();
  var logtype = $("#logtype").val();
  if (!gcnum) {
    alert("Please enter a GC number.")
    return False;
  } else {
    return "https://www.geocaching.com/play/geocache/" + gcnum + "/log?lt=" + logtype
  }
}

function usernamesearchURL() {
  var usernamebox = $('#usernamebox').val();
  var usernamelist = $("#usernamelist").val();
  if (usernamelist == "about") {
    urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" + usernamebox;
  } else if (usernamelist == "email") {
    urltoreturn = "https://www.geocaching.com/email/?u=" + usernamebox;
  } else if (usernamelist == "geocaches") {
    urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" + usernamebox + "&tab=geocaches#profilepanel";
  } else if (usernamelist == "hidden") {
    urltoreturn = "https://www.geocaching.com/seek/nearest.aspx?u=" + usernamebox;
  } else if (usernamelist == "found") {
    urltoreturn = "https://www.geocaching.com/seek/nearest.aspx?ul=" + usernamebox;
  } else if (usernamelist == "trackables") {
    urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" + usernamebox + "&tab=trackables#profilepanel";
  } else if (usernamelist == "souvenirs") {
    urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" + usernamebox + "&tab=souvenirs#profilepanel";
  } else if (usernamelist == "gallery") {
    urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" + usernamebox + "&tab=gallery#profilepanel";
  } else if (usernamelist == "lists") {
    urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" + usernamebox + "&tab=lists#profilepanel";
  } else if (usernamelist == "stats") {
    urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" + usernamebox + "&tab=stats#profilepanel";
  } else if (usernamelist == "projectgcstats") {
    urltoreturn = "https://project-gc.com/ProfileStats/" + usernamebox;
  }

  if (!usernamebox) {
    alert("Please enter a Username.")
    return False;
  } else {
    return urltoreturn;
  }
}

function gclinkURL(lat, lon) {
  var gcnum = $('#gcnum').val();
  if (!gcnum) {
    alert("Please enter a GC number.");
    return False;
  } else {
    return 'https://www.geocaching.com/geocache/' + gcnum;
  }
}

function geocachesnearbyURL(lat, lon) {
  var geocachingdistance = $('#geocachingmiles').val();
  var geocachetype = $('#geocachetype').val();
  var excludefound = "";
  if ($('#excludefound').prop('checked') == true) {
    excludefound = $('#excludefound').val();
  }

  return "https://www.geocaching.com/seek/nearest.aspx?lat=" + lat + "&lng=" + lon + "&dist=" + geocachingdistance + "&cFilter=" + geocachetype + excludefound
}

function benchmarkURL(lat, lon) {
  return 'https://www.geocaching.com/mark/nearest.aspx?lat=' + lat + "&lon=" + lon;
}

function waymarkURL(lat, lon) {
  return "http://www.waymarking.com/wm/search.aspx?f=1&lat=" + lat + "&lon=" + lon + "&t=6";
}

function calcdistURL(lat, lon) {
  return 'http://www.boulter.com/gps/distance/?from=' + lat + " " + lon;
}

function bingstreetviewURL(lat, lon) {
  return 'https://www.bing.com/maps?cp=' + lat + '~' + lon + '&lvl=17&dir=113.8911&pi=-0.284&style=x&v=2&sV=1'
}

function what3wordsmapURL() {
  var words = $('#coordinates_what3words').val();
  return 'https://map.what3words.com/' + words + '?maptype=satellite';
}

function pluscodeURL() {
  var pluscode = $('#coordinates_pluscode').val();
  return 'https://plus.codes/' + pluscode;
}

function bingURL(lat, lon) {
  return 'https://www.bing.com/maps?cp=' + lat + '~' + lon + '&lvl=17&style=a'
}

function mytopoURL(lat, lon) {
  return "http://www.mytopo.com/maps/?lat=" + lat + "&lon=" + lon + "&pid=groundspeak";
}

function openstreetmapURL(lat, lon) {
  return "https://www.openstreetmap.org/?mlat=" + lat + "&mlon=" + lon + "&zoom=17#map=17/" + lat + "/" + lon;
}

function opencyclemapURL(lat, lon) {
  return "http://www.opencyclemap.org/?zoom=15&lat=" + lat + "&lon=" + lon;
}

function geocachingmapURL(lat, lon) {
  //return 'http://www.geocaching.com/map/?ll=' + lat + "," + lon;
  return 'http://www.geocaching.com/play/map?lat=' + lat + "&lng=" + lon+"&zoom=14&asc=true&sort=distance";
}

function goTo(site) {

  var entercoords = $('#c').val();
  var entercoordsmessage = "Please enter coordinates to convert first.";

  var lat = $("#coordinates_lat_ddd").val();
  var lon = $("#coordinates_lon_ddd").val();
  var coords = $('#coordinates_pair_dmm').val();
  var URL = geocachingmapURL(lat, lon);

  if (site == "geocachingmap") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = geocachingmapURL(lat, lon);
    }
  } else if (site == "startingcoordsgoogle") {
    lat = $('#distance_calc_starting_lat_ddd').val();
    lon = $('#distance_calc_starting_lon_ddd').val();
    if (!lat) {
      alert("Please enter starting coordinates.");
      return false;
    } else {
      var coords = $('#distance_calc_starting_pair_dmm').val();
      URL = startingcoordsgoogleURL(coords);
    }
  } else if (site == "destinationcoordsgoogle") {
    lat = $('#distance_calc_destination_lat_ddd').val();
    lon = $('#distance_calc_destination_lon_ddd').val();
    if (!lat) {
      alert("Please enter destination coordinates.");
      return false;
    } else {
      var coords = $('#distance_calc_destination_pair_dmm').val();
      URL = destinationcoordsgoogleURL(coords);
    }
  } else if (site == "drivingdirectionsgoogle") {
    if (!$('#distance_calc_starting_lat_ddd').val() || !$('#distance_calc_destination_lat_ddd').val()) {
      alert("Starting and Destination coordinates required.");
      return false;
    } else {
      URL = drivingdirectionsgoogleURL();
    }
  } else if (site == "projectstartingcoordsgoogle") {
    lat = $('#projection_starting_lat_ddd').val();
    lon = $('#projection_starting_lon_ddd').val();
    if (!lat) {
      alert("Please enter starting coordinates.");
      return false;
    } else {
      var coords = $('#projection_starting_pair_dmm').val();
      URL = projectstartingcoordsgoogleURL(coords);
    }
  } else if (site == "projectdestinationcoordsgoogle") {
    lat = $('#projectedcoords_lat').val();
    lon = $('#projectedcoords_lon').val();
    if (!lat) {
      alert("Please project coordinates first.");
      return false;
    } else {
      var coords = $('#projectedcoordinates_pair_dmm').val();
      URL = projectdestinationcoordsgoogleURL(coords);
    }
  } else if (site == "projectdrivingdirectionsgoogle") {
    if (!$('#projectedcoords_lat').val() || !$('#projection_starting_lat_ddd').val()) {
      alert("Starting and Projected coordinates required.");
      return false;
    } else {
      URL = projectdrivingdirectionsgoogleURL();
    }
  } else if (site == "geocachesnearby") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = geocachesnearbyURL(lat, lon);
    }
  } else if (site == "gclink") {
    URL = gclinkURL();
  } else if (site == "geocachinglog") {
    URL = geocachinglogURL();
  } else if (site == "geocachingkeyword") {
    URL = geocachingkeywordURL();
  } else if (site == "trackables") {
    URL = trackablesURL();
  } else if (site == "usernamesearch") {
    URL = usernamesearchURL();
  } else if (site == "mapquest") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = mapquestURL(lat, lon);
    }
  } else if (site == "openstreetmap") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = openstreetmapURL(lat, lon);
    }
  } else if (site == "pluscode") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = pluscodeURL();
    }
  } else if (site == "opencyclemap") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = opencyclemapURL(lat, lon);
    }
  } else if (site == "theholygoogle") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = theholygoogleURL(coords);
    }
  } else if (site == "googlestreetview") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = googlestreetviewURL(lat, lon);
    }
  } else if (site == "bingstreetview") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = bingstreetviewURL(lat, lon);
    }
  } else if (site == "what3wordsmap") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = what3wordsmapURL(lat, lon);
    }
  } else if (site == "bing") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = bingURL(lat, lon);
    }
  } else if (site == "geourl") {
    URL = geourlURL(lat, lon);
  } else if (site == "mytopo") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = mytopoURL(lat, lon);
    }
  } else if (site == "benchmarks") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = benchmarkURL(lat, lon);
    }
  } else if (site == "waymarks") {
    if (!entercoords) {
      alert(entercoordsmessage);
      return false;
    } else {
      URL = waymarkURL(lat, lon);
    }
  } else if (site == "ftfsearch") {
    var state = $('#ftfstates option:selected').attr("id")
    if (state == "nostate"){
      alert("Please select a state or click the arrow to use your current location.")
      return false;
    } else {
      URL = ftfsearchURL(state);
    }
  }

  window.open(URL, "_blank");
}
